package com.android.sql_lite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.sql_lite.helpers.AdminSQLiteOpenHelper;

public class MainActivity extends AppCompatActivity {
    EditText etCodigo;
    EditText etPrecio;
    EditText etDescripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etCodigo = findViewById(R.id.et_code_articulo);
        etPrecio = findViewById(R.id.et_precio);
        etDescripcion = findViewById(R.id.et_descripcion);
    }

    public void guardar(View view){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(getApplicationContext(),"administracion",null,1);
        SQLiteDatabase bd = admin.getWritableDatabase(); // abriendo la bd en modo escritura y lectura

        String codigo = this.etCodigo.getText().toString();
        String precio = this.etPrecio.getText().toString();
        String descripcion = this.etDescripcion.getText().toString();

        if(codigo.isEmpty() && precio.isEmpty() && descripcion.isEmpty()){
            Toast.makeText(this, "Debes llenar todos los campos", Toast.LENGTH_SHORT).show();
            bd.close();
            return;
        }
        ContentValues register = new ContentValues();
        register.put("codigo",codigo);
        register.put("descripcion",descripcion);
        register.put("precio",precio);

        bd.insert("articulos",null,register);
        bd.close();

        etCodigo.setText("");
        etPrecio.setText("");
        etDescripcion.setText("");
        Toast.makeText(this,"Registro exitoso", Toast.LENGTH_SHORT).show();
    }

    public void consular(View view){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(getApplicationContext(),"administracion",null,1);
        SQLiteDatabase bd = admin.getWritableDatabase(); // abriendo la bd en modo escritura y lectura

        String codigo = this.etCodigo.getText().toString();

        if(codigo.isEmpty()){
            Toast.makeText(this, "Debes llenar el campo codigo", Toast.LENGTH_SHORT).show();
            bd.close();
            return;
        }
        Cursor fila = bd.rawQuery("SELECT descripcion,precio FROM articulos WHERE codigo = "+codigo,null);

        if(!fila.moveToFirst()){
            Toast.makeText(this, "El articulo "+codigo+" no existe", Toast.LENGTH_SHORT).show();
            bd.close();
            return;
        }
        etDescripcion.setText(fila.getString(0));
        etPrecio.setText(fila.getString(1));
        bd.close();

    }

    public void eliminar(View view){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(getApplicationContext(),"administracion",null,1);
        SQLiteDatabase bd = admin.getWritableDatabase(); // abriendo la bd en modo escritura y lectura

        String codigo = this.etCodigo.getText().toString();

        if(codigo.isEmpty()){
            Toast.makeText(this, "Debes llenar el campo codigo", Toast.LENGTH_SHORT).show();
            bd.close();
            return;
        }
        int eliminado = bd.delete("articulos","codigo="+codigo,null);
        bd.close();
        etCodigo.setText("");
        etPrecio.setText("");
        etDescripcion.setText("");

        if(eliminado == 1){
            Toast.makeText(this, "El articulo "+codigo+" fue eliminado", Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(this, "El articulo "+codigo+" no existe", Toast.LENGTH_SHORT).show();
    }

    public void editar(View view){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(getApplicationContext(),"administracion",null,1);
        SQLiteDatabase bd = admin.getWritableDatabase(); // abriendo la bd en modo escritura y lectura
        String codigo = this.etCodigo.getText().toString();
        String precio = this.etPrecio.getText().toString();
        String descripcion = this.etDescripcion.getText().toString();

        if(codigo.isEmpty()){
            Toast.makeText(this, "Debes el campo codigo", Toast.LENGTH_SHORT).show();
            bd.close();
            return;
        }
        ContentValues register = new ContentValues();
        register.put("codigo",codigo);
        register.put("descripcion",descripcion);
        register.put("precio",precio);


        int actualizado = bd.update("articulos",register,"codigo="+codigo,null);
        bd.close();
        if(actualizado == 1){
            Toast.makeText(this, "El articulo "+codigo+" fue actualizado", Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(this, "El articulo "+codigo+" no existe", Toast.LENGTH_SHORT).show();
    }
}